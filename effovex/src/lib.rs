pub mod error;

use error::Error;
use std::path::{Path, PathBuf};

#[derive(Clone, Copy)]
pub enum OutputSpec<'a> {
    OutputNextToInput,
    OutputInDirectory(&'a Path),
    OutputToFile(&'a Path),
}

pub fn execute_file(input_path: &Path, output_path: OutputSpec) -> Result<(), Error> {
    let parsed_file = parser::effovex_parser(input_path.to_path_buf());

    let mut ast = match parsed_file {
        Ok(ast) => ast,
        Err(err) => return Err(Error::ParserError(err)),
    };

    let output_file: PathBuf = match output_path {
        OutputSpec::OutputNextToInput => {
            let extension = ast.get_mut().doc_type.to_string();
            input_path.with_extension(extension)
        }
        OutputSpec::OutputInDirectory(dir) => {
            let extension = ast.get_mut().doc_type.to_string();
            let input_with_extension = input_path.with_extension(extension);
            let file_name = input_with_extension
                .file_name()
                .expect("was readable, so must be a file");
            dir.join(file_name)
        }
        OutputSpec::OutputToFile(file) => file.to_path_buf(),
    };

    let converted_file = converter::convert(ast, &output_file);
    let output = match converted_file {
        Ok(output) => output,
        Err(err) => return Err(Error::ConverterError(err)),
    };

    generator::print(output, &output_file);

    Ok(())
}
