use parser::error::ParserError;
use share::ConverterError;
use std::fmt::Display;

/// Enumeration grouping all possible errors that can occurr during compilation
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Error {
    ParserError(ParserError),
    ConverterError(ConverterError),
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::ParserError(err) => {
                write!(f, "Parser Error: {}", err)
            }
            Error::ConverterError(err) => {
                write!(f, "Converter Error: {}", err)
            }
        }
    }
}
