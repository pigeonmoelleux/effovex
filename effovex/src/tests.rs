use std::{assert_matches::assert_matches, fs::read_dir, path::PathBuf};

use parser::ParserError;

use crate::{error::Error, execute_file};

fn get_fx_files(dir_path: &PathBuf) -> Vec<PathBuf> {
    let mut children: Vec<PathBuf> = vec![];
    match read_dir(dir_path) {
        Ok(read_dir) => read_dir.for_each(|entry| match entry {
            Ok(child) => match child.file_type() {
                Ok(file_type) => {
                    if file_type.is_file() && child.path().extension().unwrap() == "fx" {
                        children.push(child.path())
                    } else if file_type.is_dir() {
                        children.append(&mut get_fx_files(&child.path()))
                    } else {
                        eprintln!(
                            "{:?} is neither a file nor a directory, it will not be tested",
                            child
                        )
                    }
                }
                Err(_) => panic!("Unable to fetch file type of {:?}", child),
            },
            Err(_) => panic!("Unable to fetch metadata of {:?}", entry),
        }),
        Err(_) => unreachable!("Unable to read the directory {:?}", dir_path),
    };

    return children;
}

#[test]
fn good_examples() {
    for file_path in get_fx_files(&PathBuf::from("../examples/good/")) {
        assert_eq!(
            execute_file(&file_path),
            Ok(()),
            "File tested : {:?}",
            file_path
        );
    }
}

#[test]
fn parser_error_examples() {
    for file_path in get_fx_files(&PathBuf::from(
        "../examples/bad/parser_error/already_defined_declaration/",
    )) {
        assert_matches!(
            execute_file(&file_path),
            Err(Error::ParserError(ParserError::AlreadyDefinedDeclaration(
                _,
                _,
                _,
                _
            )))
        )
    }

    for file_path in get_fx_files(&PathBuf::from("../examples/bad/parser_error/bad_env_call/")) {
        assert_matches!(
            execute_file(&file_path),
            Err(Error::ParserError(ParserError::BadEnvCall(_, _)))
        )
    }

    for file_path in get_fx_files(&PathBuf::from("../examples/bad/parser_error/cannot_parse/")) {
        assert_matches!(
            execute_file(&file_path),
            Err(Error::ParserError(ParserError::CannotParse(_, _)))
        )
    }

    for file_path in get_fx_files(&PathBuf::from(
        "../examples/bad/parser_error/missing_begin_document/",
    )) {
        assert_matches!(
            execute_file(&file_path),
            Err(Error::ParserError(ParserError::MissingBeginDocument(_)))
        )
    }

    for file_path in get_fx_files(&PathBuf::from(
        "../examples/bad/parser_error/undefined_arg/",
    )) {
        assert_matches!(
            execute_file(&file_path),
            Err(Error::ParserError(ParserError::UndefinedArg(_, _, _)))
        )
    }

    for file_path in get_fx_files(&PathBuf::from(
        "../examples/bad/parser_error/undefined_declaration/",
    )) {
        assert_matches!(
            execute_file(&file_path),
            Err(Error::ParserError(ParserError::UndefinedDeclaration(
                _,
                _,
                _
            )))
        )
    }

    for file_path in get_fx_files(&PathBuf::from(
        "../examples/bad/parser_error/wrong_signature/",
    )) {
        assert_matches!(
            execute_file(&file_path),
            Err(Error::ParserError(ParserError::WrongSignature(
                _,
                _,
                _,
                _,
                _
            )))
        )
    }
}
