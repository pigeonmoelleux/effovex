use clap::Parser;
use effovex::{error::Error, execute_file, OutputSpec};
use std::path::PathBuf;

#[derive(Parser)]
#[command(version, arg_required_else_help = true)]
struct Cli {
    files: Vec<PathBuf>,
    #[arg(short, long, help = "write output to the specified file")]
    output_file: Option<PathBuf>,
    #[arg(
        short = 'd',
        long,
        conflicts_with = "output_file",
        help = "write output with the default name in the specified directory"
    )]
    output_directory: Option<PathBuf>,
    // TODO: --show-ast for debugging
}

fn main() -> Result<(), Error> {
    let cli = Cli::parse();
    let spec = match (&cli.output_file, &cli.output_directory) {
        (&Some(_), &Some(_)) => unreachable!("prevented by conflicts_with"),
        (&Some(ref file), &None) => OutputSpec::OutputToFile(file),
        (&None, &Some(ref dir)) => OutputSpec::OutputInDirectory(dir),
        (&None, &None) => OutputSpec::OutputNextToInput,
    };
    for file in cli.files {
        match execute_file(&file, spec) {
            Ok(()) => {}
            Err(err) => eprintln!("{}", err),
        };
    }
    Ok(())
}
