#[macro_use]
extern crate lazy_static;

use std::collections::HashMap;
use std::fmt::Write as FmtWrite;
use std::fmt::{Debug, Error as FmtError, Formatter};
use std::fs::{create_dir_all, metadata, read_to_string, remove_dir_all, File};
use std::io::{BufReader, Read, Write};
use std::path::{Path, PathBuf};

use effovex::{error::Error as EffovexError, execute_file, OutputSpec};
use globwalk::{FileType, GlobWalkerBuilder};
use html_builder::{Buffer, Html5, Node};
use parser::ParserError;
use pathdiff::diff_paths;
use share::ConverterError;
use similar::{ChangeTag, TextDiff};

lazy_static! {
    static ref ERROR_CHECKERS: HashMap<&'static str, fn(&Result<(), EffovexError>) -> Result<(), ()>> = {
        let mut checkers = HashMap::new();
        macro_rules! define_checker {
            ($e:literal, $pat:pat) => {
                let closure: fn(&Result<(), EffovexError>) -> Result<(), ()> = |x| match x {
                    $pat => Ok(()),
                    _ => Err(()),
                };
                checkers.insert($e, closure);
            };
        }
        define_checker!("good", Ok(()));
        define_checker!(
            "bad/parser_error/already_defined_declaration",
            Err(EffovexError::ParserError(
                ParserError::AlreadyDefinedDeclaration(_, _, _, _,)
            ))
        );
        define_checker!(
            "bad/parser_error/bad_env_call",
            Err(EffovexError::ParserError(ParserError::BadEnvCall(_, _)))
        );
        define_checker!(
            "bad/parser_error/cannot_parse",
            Err(EffovexError::ParserError(ParserError::CannotParse(_, _)))
        );
        define_checker!(
            "bad/parser_error/file_not_readable",
            Err(EffovexError::ParserError(ParserError::FileNotReadable(_)))
        );
        define_checker!(
            "bad/converter_error/undefined_arg",
            Err(EffovexError::ConverterError(ConverterError::UndefinedArg(
                _,
                _,
                _
            )))
        );
        define_checker!(
            "bad/converter_error/undefined_declaration",
            Err(EffovexError::ConverterError(
                ConverterError::UndefinedDeclaration(_, _, _,)
            ))
        );
        define_checker!(
            "bad/parser_error/missing_begin_document",
            Err(EffovexError::ParserError(
                ParserError::MissingBeginDocument(_)
            ))
        );
        define_checker!(
            "bad/parser_error/wrong_signature",
            Err(EffovexError::ParserError(ParserError::WrongSignature(
                _,
                _,
                _,
                _,
                _,
            )))
        );
        define_checker!(
            "bad/converter_error/wrong_signature",
            Err(EffovexError::ConverterError(
                ConverterError::WrongSignature(_, _, _, _, _,)
            ))
        );
        define_checker!(
            "bad/converter_error/package_error",
            Err(EffovexError::ConverterError(ConverterError::PackageError(
                _,
                _
            )))
        );
        checkers
    };
}

/// Easy error handling.

struct ScriptError(String);

impl Debug for ScriptError {
    fn fmt(&self, formatter: &mut Formatter) -> Result<(), FmtError> {
        let ScriptError(msg) = self;
        write!(formatter, "error occurred while testing: {}", msg)
    }
}

impl<T> From<T> for ScriptError
where
    T: ToString,
{
    fn from(error: T) -> ScriptError {
        ScriptError(error.to_string())
    }
}

type ScriptResult<T> = Result<T, ScriptError>;

/// Find all files with extension `ext` in the given directory.
fn glob<P: AsRef<Path>, S: AsRef<str>>(dir: P, pat: S) -> ScriptResult<Vec<PathBuf>> {
    Ok(GlobWalkerBuilder::new(dir, pat)
        .file_type(FileType::FILE)
        .build()?
        .map(|er| er.map(|e| e.into_path()))
        .collect::<Result<Vec<_>, _>>()?)
}

fn check_status_against_rules(path: &Path, res: &Result<(), EffovexError>) -> ScriptResult<()> {
    if let None = path.to_str() {
        Err(format!("non-UTF-8 path: {}", path.to_string_lossy()))?;
    }
    for p in path.ancestors() {
        let s = p.to_str().expect("valid UTF-8 because the whole path is");
        if let Some(checker) = ERROR_CHECKERS.get(s) {
            match checker(res) {
                Err(()) => {
                    let mut msg = String::new();
                    msg += &match res {
                        Err(fx_err) => {
                            format!("error for test {} is unexpected: {}.", s, fx_err)
                        }
                        Ok(()) => format!("test {} unexpectedly succeeded!", s),
                    };
                    msg.push_str(
                        " Please fix the problem or change the expected error \
                                  near the beginning of effovex/tests/build_and_compare.rs",
                    );
                    Err(msg)?
                }
                Ok(()) => return Ok(()),
            }
        }
    }
    Err(format!(
        "no checker found for {}, please add one at the top of effovex/tests/build_and_compare.rs",
        path.to_str().expect("already checked")
    ))?
}

/// Compile all tests, capturing .png output and log.
fn compile_tests(run: &TestRun) -> ScriptResult<()> {
    if run.output_dir.is_dir() {
        remove_dir_all(&run.output_dir)?;
    }
    let examples_dir = run.toplevel_dir.join("examples");
    glob(&examples_dir, "**/*.fx")?
        .into_iter()
        .try_for_each(|path: PathBuf| -> ScriptResult<()> {
            // examples/good/example_1.fx -> good/example_1
            let test_name = path
                .strip_prefix(&examples_dir)
                .expect("path is in examples/, should have examples/ prefix")
                .with_extension("");
            eprintln!("Processing {}...", test_name.to_string_lossy());
            let test_parent_name = test_name
                .parent()
                .expect("test is somewhere inside the examples/ dir");
            let nested_output_dir = run.output_dir.join(test_parent_name);
            // Create directory if it doesn't exist.
            create_dir_all(&nested_output_dir)?;
            // Compile file.
            let file_res = execute_file(&path, OutputSpec::OutputInDirectory(&nested_output_dir));
            // Write log.
            // TODO: implement effovex option to write log to a separate file?
            let log_path = run.output_dir.join(&test_name).with_extension("log");
            let mut log_file = File::create(log_path)?;
            match file_res {
                Ok(()) => log_file.write(b"")?,
                Err(ref err) => log_file.write(err.to_string().as_bytes())?,
            };
            // Run error test.
            check_status_against_rules(&test_name, &file_res)?;
            Ok(())
        })
}

/// Information about the build for a given test, either in the baseline or in
/// the comparison directory.
struct BuiltTestInfo {
    /// Path to `.log` file.
    log_path: PathBuf,
    /// Optional to `.png` output.
    png_path: Option<PathBuf>,
}

impl BuiltTestInfo {
    fn all_in_directory(dir: &Path) -> ScriptResult<Vec<Self>> {
        // Assume there is no .png file without a corresponding .log file.
        let vec = glob(dir, "**/*.log")?
            .into_iter()
            .map(|log_path: PathBuf| {
                let png_path: PathBuf = log_path.with_extension("png");
                BuiltTestInfo {
                    log_path: log_path,
                    png_path: if png_path.is_file() {
                        Some(png_path)
                    } else {
                        None
                    },
                }
            })
            .collect();
        Ok(vec)
    }
}

/// All information about a given test in the baseline and the comparison
/// directory.
struct ComparisonInfo {
    test_name: PathBuf,
    baseline_info: Option<BuiltTestInfo>,
    comparison_info: Option<BuiltTestInfo>,
}

impl ComparisonInfo {
    fn all_for_directories(
        baseline_dir: &Path,
        comparison_dir: &Path,
    ) -> ScriptResult<Vec<ComparisonInfo>> {
        let mut test_name_to_info: HashMap<PathBuf, ComparisonInfo> = HashMap::new();
        let baseline_infos = BuiltTestInfo::all_in_directory(baseline_dir)?;
        let comparison_infos = BuiltTestInfo::all_in_directory(comparison_dir)?;
        baseline_infos.into_iter().for_each(|baseline_info| {
            let test_name = baseline_info
                .log_path
                .strip_prefix(baseline_dir)
                .expect("path was found in baseline")
                .with_extension("");
            let cmp_info = ComparisonInfo {
                test_name: test_name.clone(),
                baseline_info: Some(baseline_info),
                comparison_info: None,
            };
            test_name_to_info.insert(test_name, cmp_info);
        });
        let mut res: Vec<ComparisonInfo> = comparison_infos
            .into_iter()
            .map(|comparison_info| {
                let test_name = comparison_info
                    .log_path
                    .strip_prefix(comparison_dir)
                    .expect("path was found in comparison")
                    .with_extension("");
                match test_name_to_info.remove(&test_name) {
                    None => ComparisonInfo {
                        test_name,
                        baseline_info: None,
                        comparison_info: Some(comparison_info),
                    },
                    Some(cmp_info) => ComparisonInfo {
                        comparison_info: Some(comparison_info),
                        ..cmp_info
                    },
                }
            })
            .collect();
        res.extend(&mut test_name_to_info.into_values());
        Ok(res)
    }
}

fn pretty_diff(log1: String, log2: String, out: &mut Node) -> ScriptResult<()> {
    let diff = TextDiff::from_lines(&log1, &log2);
    for change in diff.iter_all_changes() {
        let (color, tag) = match change.tag() {
            ChangeTag::Delete => ("#ff0000", "- "),
            ChangeTag::Insert => ("#00ff00", "+ "),
            ChangeTag::Equal => ("#4b0082", "= "),
        };
        let mut node = out.span().attr(&format!("style=\"color:{}\"", color));
        writeln!(node, "{}{}", tag, change)?;
    }
    Ok(())
}

fn binary_files_equal(f1: &Path, f2: &Path) -> ScriptResult<bool> {
    let m1 = metadata(f1)?;
    let m2 = metadata(f2)?;
    if m1.len() != m2.len() {
        return Ok(false);
    }
    let mut buffer1 = [0; 1024];
    let mut buffer2 = [0; 1024];
    let file1 = File::open(f1)?;
    let mut reader1 = BufReader::new(file1);
    let file2 = File::open(f2)?;
    let mut reader2 = BufReader::new(file2);
    let res = loop {
        let count1 = reader1.read(&mut buffer1)?;
        let count2 = reader2.read(&mut buffer2)?;
        if count1 == 0 && count2 == 0 {
            break true;
        }
        if buffer1 != buffer2 {
            break false;
        }
    };
    Ok(res)
}

fn gen_html<'a>(run: &TestRun, infos: &'a Vec<ComparisonInfo>) -> ScriptResult<String> {
    let mut buf = Buffer::new();
    let mut html = buf.html().attr("lang='en'");
    let mut head = html.head();
    head.meta().attr("charset=\"utf-8\"");
    writeln!(head.title(), "Comparison of Effovex regression tests")?;
    let mut body: Node = html.body();
    writeln!(body.h1(), "Comparison of Effovex regression tests")?;
    let mut one_test_changed = false;
    for ComparisonInfo {
        test_name,
        baseline_info,
        comparison_info,
    } in infos
    {
        eprintln!("Comparing {}", test_name.to_string_lossy());
        let log_str = |maybe_test_info: &Option<BuiltTestInfo>| {
            if let Some(test_info) = maybe_test_info {
                read_to_string(&test_info.log_path)
            } else {
                Ok(String::new())
            }
        };
        let str1 = log_str(&baseline_info)?;
        let str2 = log_str(&comparison_info)?;
        let log_changed = str1 != str2;
        let img_path = |maybe_test_info: &'a Option<BuiltTestInfo>| -> Option<&'a Path> {
            match maybe_test_info {
                // Maybe the test was added or removed.  Maybe it failed to
                // compile so there is no image.
                Some(BuiltTestInfo {
                    png_path: Some(png_path),
                    ..
                }) => Some(png_path),
                _ => None,
            }
        };
        let img1_path: Option<&Path> = img_path(&baseline_info);
        let img2_path: Option<&Path> = img_path(&comparison_info);
        let image_changed = match (img1_path, img2_path) {
            (None, None) => false,
            (Some(_), None) | (None, Some(_)) => true,
            (Some(path1), Some(path2)) => !binary_files_equal(path1, path2)?,
        };
        let test_changed = log_changed || image_changed;
        if test_changed {
            one_test_changed = true;
            {
                let mut para = body.p();
                para.b().write_str("File: ")?;
                para.code()
                    .write_str(&test_name.with_extension("fx").to_string_lossy())?;
            }
            {
                let mut para = body.p();
                para.b().write_str("Log:")?;
                pretty_diff(str1, str2, &mut para.pre())?;
            }
            {
                let mut table = body.table().attr("style=\"border: 1px solid black\"");
                {
                    let mut header_row = table.tr();
                    header_row.th().write_str("Old")?;
                    header_row.th().write_str("New")?;
                }
                {
                    let mut img_row = table.tr();
                    for img_path in [&img1_path, &img2_path] {
                        let mut dat = img_row.td();
                        match img_path {
                            None => {
                                dat.p().write_str("No image to show")?;
                            }
                            Some(img_path) => {
                                // TODO: don't panic
                                let rel_path = diff_paths(img_path, &run.output_dir)
                                    .expect("failed to diff paths?");
                                // Strange that raster doesn't accept &Path.
                                let rel_path_name = rel_path.to_str().expect("non-UTF-8 path");
                                dat.img().attr(&format!("src=\"{}\"", rel_path_name));
                            }
                        }
                    }
                }
            }
        }
    }
    if !one_test_changed {
        writeln!(body.p(), "No differences found.")?;
    }
    Ok(buf.finish())
}

/// General information about the test run.
struct TestRun {
    toplevel_dir: PathBuf,
    baseline_dir: Option<PathBuf>,
    output_dir: PathBuf,
}

fn run_comparison(run: &TestRun) -> ScriptResult<()> {
    if let Some(ref baseline_dir) = run.baseline_dir {
        let infos = ComparisonInfo::all_for_directories(baseline_dir, &run.output_dir)?;
        let html = gen_html(run, &infos)?;
        let html_file = run.output_dir.join("index.html");
        File::create(&html_file)?.write(html.as_bytes())?;
    }
    Ok(())
}

fn main() -> ScriptResult<()> {
    let run = {
        let toplevel_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("..");
        let baseline = envmnt::is_or("EFFOVEX_BASELINE", false);
        let output_dir = if baseline {
            toplevel_dir.join("baseline")
        } else {
            toplevel_dir.join("comparison")
        };
        let baseline_dir = if baseline {
            None
        } else {
            let dir = toplevel_dir.join("baseline");
            if !dir.is_dir() {
                Err(String::from("Missing baseline for comparison. Please run \"EFFOVEX_BASELINE=1 cargo test\" on master first.",))?
            }
            Some(dir)
        };
        TestRun {
            toplevel_dir,
            baseline_dir,
            output_dir,
        }
    };
    compile_tests(&run)?;
    run_comparison(&run)?;
    Ok(())
}
