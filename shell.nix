with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "env";
  nativeBuildInputs = [ pkg-config vscode-extensions.matklad.rust-analyzer ];
  buildInputs = [
    cairo
    glib
    gobject-introspection
  ];
  
  RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
}