use std::collections::HashMap;

use parser::{Declaration, Showable, ShowableType, AST};
use parser::{DeclarationType, Loc};
use share::converter_error::ConverterError;

use crate::structs::AbstractDeclaration;

/// Check that the given AST is correct.
///
/// It checks that :
///
/// * the abstract arguments used in the function and environment declarations are defined before being used
/// * the functions and environment used are defined when being called
/// * the number of arguments used is the same as the one given in the declaration
pub fn verifier(
    ast: &AST,
    converted_declarations: &HashMap<(DeclarationType, String), AbstractDeclaration>,
) -> Result<(), ConverterError> {
    let mut declaration_signatures: HashMap<(DeclarationType, String), (usize, Loc)> =
        HashMap::new();
    ast
        .declarations
        .iter()
        .try_for_each(|((_, _), declaration)| {
            match declaration {
                Declaration::DefFunc(loc, name, args, _) => {
                    match declaration_signatures.insert(
                        (DeclarationType::Function, name.to_owned()),
                        (args.len(), loc.clone()),
                    ) {
                        None => {}
                        Some(_) => unreachable!("Already Defined Declaration Error happened but has already been tested")
                    }
                }
                Declaration::DefEnv(loc, name, args, _, _) => {
                    match declaration_signatures.insert(
                        (DeclarationType::Environment, name.to_owned()),
                        (args.len(), loc.clone()),
                    ) {
                        None => {}
                        Some(_) => unreachable!("Already Defined Declaration Error happened but has already been tested")
                    }
                }
            };
            Ok(())
        })?;

    for ((_, _), declaration) in &ast.declarations {
        match declarations_verifier(declaration, &declaration_signatures, converted_declarations) {
            Ok(()) => {}
            Err(err) => return Err(err),
        }
    }

    for showable in &ast.shown {
        match showable_verifier(
            showable,
            &[],
            "document".to_owned(),
            &declaration_signatures,
            converted_declarations,
        ) {
            Ok(()) => {}
            Err(err) => return Err(err),
        }
    }

    Ok(())
}

/// Check that the given shown object is correct regarding to the declarations given
fn showable_verifier(
    showable: &Showable,
    args: &[String],
    declaration_name: String,
    declaration_signatures: &HashMap<(DeclarationType, String), (usize, Loc)>,
    converted_declarations: &HashMap<(DeclarationType, String), AbstractDeclaration>,
) -> Result<(), ConverterError> {
    match &showable.showable_type {
        ShowableType::AbstractArg(name) => {
            if !args.contains(name) {
                return Err(ConverterError::UndefinedArg(
                    showable.location.clone(),
                    name.to_owned(),
                    declaration_name,
                ));
            }
        }
        ShowableType::CalledEnv(name, called_args, shown) => {
            let (nb_args, _) = match declaration_signatures
                .get(&(DeclarationType::Environment, name.to_owned()))
            {
                None => {
                    match converted_declarations
                        .get(&(DeclarationType::Environment, name.to_owned()))
                    {
                        None => {
                            return Err(ConverterError::UndefinedDeclaration(
                                showable.location.clone(),
                                DeclarationType::Environment,
                                name.to_owned(),
                            ))
                        }
                        Some(_) => (called_args.len(), Loc::default()),
                    }
                }
                Some((nb_args, loc)) => (*nb_args, loc.clone()),
            };
            if nb_args != called_args.len() {
                return Err(ConverterError::WrongSignature(
                    showable.location.clone(),
                    DeclarationType::Environment,
                    name.to_owned(),
                    nb_args,
                    called_args.len(),
                ));
            }
            called_args.iter().try_for_each(|arg| {
                arg.iter().try_for_each(|showable| {
                    showable_verifier(
                        showable,
                        args,
                        declaration_name.to_owned(),
                        declaration_signatures,
                        converted_declarations,
                    )
                })
            })?;
            shown.iter().try_for_each(|showable| {
                showable_verifier(
                    showable,
                    args,
                    declaration_name.to_owned(),
                    declaration_signatures,
                    converted_declarations,
                )
            })?;
        }
        ShowableType::CalledFunc(name, args) => {
            let (nb_args, _) = match declaration_signatures
                .get(&(DeclarationType::Function, name.to_owned()))
            {
                None => {
                    match converted_declarations.get(&(DeclarationType::Function, name.to_owned()))
                    {
                        None => {
                            return Err(ConverterError::UndefinedDeclaration(
                                showable.location.clone(),
                                DeclarationType::Function,
                                name.to_owned(),
                            ));
                        }
                        Some(_) => (args.len(), Loc::default()),
                    }
                }
                Some((nb_args, loc)) => (*nb_args, loc.clone()),
            };
            if nb_args != args.len() {
                return Err(ConverterError::WrongSignature(
                    showable.location.clone(),
                    DeclarationType::Environment,
                    name.to_owned(),
                    nb_args,
                    args.len(),
                ));
            }
        }
        ShowableType::Newline => {}
        ShowableType::Text(_) => {}
    }

    Ok(())
}

/// Check that the given declaration is correct
///
/// Returns the signature (declaration type, name of the declaration, number of arguments)
fn declarations_verifier(
    declaration: &Declaration,
    declaration_signatures: &HashMap<(DeclarationType, String), (usize, Loc)>,
    converted_declarations: &HashMap<(DeclarationType, String), AbstractDeclaration>,
) -> Result<(), ConverterError> {
    match declaration {
        Declaration::DefFunc(_, name, args, returned) => {
            for showable in returned {
                showable_verifier(
                    showable,
                    args,
                    name.to_owned(),
                    declaration_signatures,
                    converted_declarations,
                )?;
            }
        }
        Declaration::DefEnv(_, name, args, beg, end) => {
            for showable in beg {
                showable_verifier(
                    showable,
                    args,
                    name.to_owned(),
                    declaration_signatures,
                    converted_declarations,
                )?;
            }
            for showable in end {
                showable_verifier(
                    showable,
                    args,
                    name.to_owned(),
                    declaration_signatures,
                    converted_declarations,
                )?;
            }
        }
    }
    Ok(())
}
