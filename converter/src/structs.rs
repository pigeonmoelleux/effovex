use std::any::Any;
use std::{collections::HashMap, fmt::Debug};

use cairo::{Context, Rectangle};
use parser::{Declaration, DeclarationType, Showable};
use share::converter_error::ConverterError;
use share::{Displayable, EnvironmentEncapsulation};

type GenericFn = fn(
    &Vec<Box<dyn Displayable>>,
    &Context,
    &HashMap<(DeclarationType, String), Declaration>,
    &HashMap<(DeclarationType, String), AbstractDeclaration>,
    &Declaration,
    Option<&Vec<Showable>>,
    &mut EnvironmentEncapsulation,
) -> Result<Box<dyn Displayable>, ConverterError>;

type ExternFn = fn(
    &Vec<Box<dyn Displayable>>,
    &mut EnvironmentEncapsulation,
) -> Result<Box<dyn Displayable>, ConverterError>;

type EnvFn = fn(
    &Vec<Box<dyn Displayable>>,
    &Vec<Box<dyn Displayable>>,
    &mut EnvironmentEncapsulation,
) -> Result<Box<dyn Displayable>, ConverterError>;

/// Abstract declaration type needed to load functions and environments from packages
pub enum AbstractDeclaration {
    /// Corresponds to the generic type for declarations defined in Effovex
    Generic(GenericFn),

    /// Corresponds to a function from a package
    ExternFunction(ExternFn),

    /// Corresponds to an environment from a package
    ExternEnvironment(EnvFn),
}

#[derive(Clone, Debug)]
pub struct Newline;

/// Structure produced by the converter, it got
/// mostly the same fields as the parser AST, but
/// objects now implement `Displayable`
pub struct PrintableAST {
    /// The Cairo context of the document
    pub context: Context,

    /// The type of document, inherited from the parser AST
    pub doctype: parser::Type,

    /// All shown objects
    pub shown: Vec<Box<dyn Displayable>>,
}

impl Displayable for Newline {
    fn area(&self) -> Rectangle {
        Rectangle::new(0., 0., 0., 0.)
    }

    fn starting_point(&self) -> (f64, f64) {
        (0., 0.)
    }

    fn display(
        &self,
        context: &Context,
        start: (f64, f64),
        _: bool,
        line_contents: &mut Vec<Box<dyn Displayable>>,
        rest: &mut Vec<Box<dyn Displayable>>,
    ) -> (f64, f64) {
        let current_line_low_point =
            line_contents.area().height() - line_contents.starting_point().1;
        let next_line = {
            let mut accu = vec![];
            for obj in rest {
                if obj.to_owned().type_id() != self.type_id() {
                    accu.push(obj.to_owned());
                } else {
                    break;
                }
            }
            accu
        };
        let next_line_high_point = next_line.starting_point().1;
        context.move_to(40., start.1 + current_line_low_point);
        context.rel_move_to(0., next_line_high_point);
        context.current_point().unwrap()
    }

    fn clone_dyn(&self) -> Box<dyn Displayable> {
        Box::new(self.clone())
    }
}
