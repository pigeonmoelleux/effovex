use std::{fmt::Debug, path::PathBuf};

use libloading::{Library, Symbol};
use parser::DeclarationType;

use crate::{error::ConverterError, Displayable};

/// Trait for loaded packages
pub trait Package<'a>: Debug {
    /// Returns the name of the package
    fn path(&self) -> &'static str;

    fn declaration_signature(&self) -> (DeclarationType, String);

    extern "C" fn declaration(
        &self,
        args: &Vec<Box<dyn Displayable>>,
        inner_showable: Option<&Vec<Box<dyn Displayable>>>,
    ) -> *mut Box<Box<dyn Displayable>>;
}

/// Storage for all packages and libraries
pub struct PackageManager<'a> {
    pub packages: Vec<Box<dyn Package<'a>>>,
    pub loaded_libraries: Vec<Library>,
}

impl<'a> PackageManager<'a> {
    /// Create a package from its path
    pub unsafe fn load_package(&mut self, path: PathBuf) -> Result<(), ConverterError> {
        let lib = match Library::new(&path) {
            Ok(lib) => lib,
            Err(_) => return Err(ConverterError::PackageLoadingFailed(path)),
        };

        let constructor: Symbol<fn() -> *mut dyn Package<'a>> = match lib.get(b"_package_create") {
            Ok(symbol) => symbol,
            Err(_) => return Err(ConverterError::CannotCreatePackage(path)),
        };

        let package = Box::from_raw(constructor());

        self.loaded_libraries.push(lib);
        self.packages.push(package);

        return Ok(());
    }
}

#[macro_export]
macro_rules! declare_package {
    ($plugin_type:ty, $constructor:path) => {
        #[no_mangle]
        pub extern "C" fn _package_create() -> *mut dyn $crate::Package<'static> {
            let constructor: fn() -> $plugin_type = $constructor;

            let object = constructor();
            let boxed: Box<dyn $crate::Package> = Box::new(object);
            Box::into_raw(boxed)
        }
    };
}
