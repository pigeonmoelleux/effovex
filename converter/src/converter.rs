#![allow(clippy::ptr_arg)]

use std::cell::RefCell;
use std::collections::HashMap;
use std::path::{Path, PathBuf};

use cairo::{Context, Format, ImageSurface, PdfSurface, SvgSurface};
use pango::{units_from_double, FontDescription};

use parser::{Declaration, DeclarationType, Loc, Showable, ShowableType, Type, AST};
use share::{
    vec_displayable_cast, vec_vec_displayable_cast, ConverterError, Displayable,
    EnvironmentEncapsulation,
};
use stdlib::{frac, maths};

use crate::structs::AbstractDeclaration;
use crate::PrintableAST;
use crate::{verifier, Newline};

pub const WIDTH: f64 = 595.;
pub const HEIGHT: f64 = 842.;

fn generic_abstract_arg_replacement(
    abstract_args_values: &Vec<Box<dyn Displayable>>,
    context: &Context,
    declarations: &HashMap<(DeclarationType, String), Declaration>,
    processed_declarations: &HashMap<(DeclarationType, String), AbstractDeclaration>,
    declaration: &Declaration,
    inner_arg: Option<&Vec<Showable>>,
    environment_encapsulation: &mut EnvironmentEncapsulation,
) -> Result<Box<dyn Displayable>, ConverterError> {
    fn add_vec_showable(
        showable_list: &Vec<Showable>,
        abstract_args_values: &Vec<Box<dyn Displayable>>,
        context: &Context,
        declarations: &HashMap<(DeclarationType, String), Declaration>,
        processed_declarations: &HashMap<(DeclarationType, String), AbstractDeclaration>,
        abstract_arg_names: &Vec<String>,
        environment_encapsulation: &mut EnvironmentEncapsulation,
    ) -> Result<Vec<Box<dyn Displayable>>, ConverterError> {
        let mut new_shown = vec![];
        for obj in showable_list {
            let new_obj: Box<dyn Displayable> = match &obj.showable_type {
                ShowableType::AbstractArg(arg) => abstract_args_values
                    .get(match &abstract_arg_names.iter().position(|a| a == arg) {
                        None => unreachable!(""),
                        Some(index) => *index,
                    })
                    .unwrap()
                    .clone(),
                _ => convert_obj(
                    obj,
                    context,
                    declarations,
                    processed_declarations,
                    environment_encapsulation,
                )?,
            };
            new_shown.push(new_obj);
        }
        Ok(new_shown)
    }

    match declaration {
        Declaration::DefFunc(_, _, args, returned) => {
            return Ok(vec_displayable_cast(add_vec_showable(
                returned,
                abstract_args_values,
                context,
                declarations,
                processed_declarations,
                args,
                environment_encapsulation,
            )?));
        }
        Declaration::DefEnv(_, _, args, beg, end) => {
            return Ok(vec_displayable_cast(
                [
                    add_vec_showable(
                        beg,
                        abstract_args_values,
                        context,
                        declarations,
                        processed_declarations,
                        args,
                        environment_encapsulation,
                    )?,
                    add_vec_showable(
                        inner_arg.unwrap(),
                        abstract_args_values,
                        context,
                        declarations,
                        processed_declarations,
                        args,
                        environment_encapsulation,
                    )?,
                    add_vec_showable(
                        end,
                        abstract_args_values,
                        context,
                        declarations,
                        processed_declarations,
                        args,
                        environment_encapsulation,
                    )?,
                ]
                .concat(),
            ));
        }
    }
}

fn convert_obj(
    obj: &Showable,
    context: &Context,
    declarations: &HashMap<(DeclarationType, String), Declaration>,
    processed_declarations: &HashMap<(DeclarationType, String), AbstractDeclaration>,
    environment_encapsulation: &mut EnvironmentEncapsulation,
) -> Result<Box<dyn Displayable>, ConverterError> {
    fn convert_list(
        showable_list: &Vec<Showable>,
        context: &Context,
        declarations: &HashMap<(DeclarationType, String), Declaration>,
        processed_declarations: &HashMap<(DeclarationType, String), AbstractDeclaration>,
        environment_encapsulation: &mut EnvironmentEncapsulation,
    ) -> Result<Vec<Box<dyn Displayable>>, ConverterError> {
        let mut converted_list = vec![];
        for showable in showable_list {
            match convert_obj(
                showable,
                context,
                declarations,
                processed_declarations,
                environment_encapsulation,
            ) {
                Ok(obj) => converted_list.push(obj),
                Err(err) => return Err(err),
            }
        }
        Ok(converted_list)
    }

    fn convert_list_list(
        showable_list_list: &Vec<Vec<Showable>>,
        context: &Context,
        declarations: &HashMap<(DeclarationType, String), Declaration>,
        processed_declarations: &HashMap<(DeclarationType, String), AbstractDeclaration>,
        environment_encapsulation: &mut EnvironmentEncapsulation,
    ) -> Result<Vec<Vec<Box<dyn Displayable>>>, ConverterError> {
        let mut converted_list = vec![];
        for showable_list in showable_list_list {
            match convert_list(
                showable_list,
                context,
                declarations,
                processed_declarations,
                environment_encapsulation,
            ) {
                Ok(obj) => converted_list.push(obj),
                Err(err) => return Err(err),
            }
        }
        Ok(converted_list)
    }

    match &obj.showable_type {
        ShowableType::Text(str) => {
            let mut font = FontDescription::new();
            font.set_family("Georgia");
            font.set_size(units_from_double(12.));

            let layout = pangocairo::create_layout(context);
            layout.set_text(str as &str);
            layout.set_justify(true);
            layout.set_font_description(Some(&font));
            layout.set_width(units_from_double(WIDTH - 80.));

            Ok(Box::new(layout))
        }
        ShowableType::Newline => Ok(Box::new(Newline)),
        ShowableType::CalledFunc(name, args) => {
            let fun = processed_declarations
                .get(&(DeclarationType::Function, name.to_string()))
                .expect("Unattainable location: should exist");

            Ok(match fun {
                AbstractDeclaration::Generic(f) => f(
                    &vec_vec_displayable_cast(convert_list_list(
                        args,
                        context,
                        declarations,
                        processed_declarations,
                        environment_encapsulation,
                    )?),
                    context,
                    declarations,
                    processed_declarations,
                    declarations
                        .get(&(DeclarationType::Function, name.to_owned()))
                        .unwrap(),
                    None,
                    environment_encapsulation,
                )?,
                AbstractDeclaration::ExternFunction(f) => f(
                    &vec_vec_displayable_cast(convert_list_list(
                        args,
                        context,
                        declarations,
                        processed_declarations,
                        environment_encapsulation,
                    )?),
                    environment_encapsulation,
                )?,
                AbstractDeclaration::ExternEnvironment(_) => {
                    return Err(ConverterError::BadDeclarationType(
                        name.to_owned(),
                        DeclarationType::Function,
                        DeclarationType::Environment,
                    ))
                }
            })
        }
        ShowableType::CalledEnv(name, args, shown) => {
            environment_encapsulation.push(name.to_owned());
            let env = processed_declarations
                .get(&(DeclarationType::Environment, name.to_string()))
                .expect("Unattainable location: should exist");
            let result = Ok(match env {
                AbstractDeclaration::Generic(f) => f(
                    &vec_vec_displayable_cast(convert_list_list(
                        args,
                        context,
                        declarations,
                        processed_declarations,
                        environment_encapsulation,
                    )?),
                    context,
                    declarations,
                    processed_declarations,
                    declarations
                        .get(&(DeclarationType::Environment, name.to_owned()))
                        .unwrap(),
                    Some(shown),
                    environment_encapsulation,
                )?,
                AbstractDeclaration::ExternFunction(_) => {
                    return Err(ConverterError::BadDeclarationType(
                        name.to_owned(),
                        DeclarationType::Function,
                        DeclarationType::Environment,
                    ))
                }
                AbstractDeclaration::ExternEnvironment(f) => f(
                    &vec_vec_displayable_cast(convert_list_list(
                        args,
                        context,
                        declarations,
                        processed_declarations,
                        environment_encapsulation,
                    )?),
                    &convert_list(
                        shown,
                        context,
                        declarations,
                        processed_declarations,
                        environment_encapsulation,
                    )?,
                    environment_encapsulation,
                )?,
            });
            environment_encapsulation.pop();
            result
        }
        ShowableType::AbstractArg(_) => unimplemented!("There should not be an abstract arg here."),
    }
}

pub fn convert(
    ref_tree: RefCell<AST>,
    path: &Path,
) -> Result<RefCell<PrintableAST>, ConverterError> {
    let tree = ref_tree.into_inner();

    let context = match tree.doc_type {
        Type::PDF => {
            let target =
                PdfSurface::new(WIDTH, HEIGHT, path).expect("Failed to create PDF Surface");
            Context::new(target).expect("Failed to create Cairo Context")
        }
        Type::PNG => {
            let target = ImageSurface::create(Format::ARgb32, WIDTH as i32, HEIGHT as i32)
                .expect("Failed to create PNG Surface");
            let context = Context::new(target).expect("Failed to create Cairo Context");

            context.set_source_rgb(255., 255., 255.);
            context.paint().expect("Failed to fill page");
            context.set_source_rgb(0., 0., 0.);

            context
        }
        Type::SVG => {
            let target =
                SvgSurface::new(WIDTH, HEIGHT, Some(path)).expect("Failed to create SVG Surface");
            Context::new(target).expect("Failed to create Cairo Context")
        }
    };

    let mut converted_declarations: HashMap<(DeclarationType, String), AbstractDeclaration> =
        HashMap::new();

    for ((declaration_type, name), declaration) in &tree.declarations {
        match converted_declarations.insert(
            (declaration_type.clone(), name.to_owned()),
            AbstractDeclaration::Generic(generic_abstract_arg_replacement),
        ) {
            None => {}
            Some(_) => {
                return Err(ConverterError::AlreadyDefinedDeclaration(
                    declaration.location(),
                    Loc::default(),
                    DeclarationType::from(declaration),
                    declaration.name(),
                ))
            }
        }
    }

    for package in &tree.imported_packages {
        match package.as_str() {
            "maths" => {
                converted_declarations.insert(
                    (DeclarationType::Function, "frac".to_owned()),
                    AbstractDeclaration::ExternFunction(frac),
                );
                converted_declarations.insert(
                    (DeclarationType::Environment, "maths".to_owned()),
                    AbstractDeclaration::ExternEnvironment(maths),
                )
            }
            _ => return Err(ConverterError::PackageLoadingFailed(PathBuf::from(package))),
        };
    }

    // Verifies that the given AST is correct
    match verifier(&tree, &converted_declarations) {
        Ok(()) => {}
        Err(err) => return Err(err),
    };

    let mut environment_encapsulation: EnvironmentEncapsulation = vec![];

    // Creation of the shown objects, implementing `Displayable`
    let mut new_shown = Vec::new();
    for obj in tree.shown {
        new_shown.push(convert_obj(
            &obj,
            &context,
            &tree.declarations,
            &converted_declarations,
            &mut environment_encapsulation,
        )?);
    }

    Ok(RefCell::new(PrintableAST {
        context,
        doctype: tree.doc_type,
        shown: new_shown,
    }))
}
