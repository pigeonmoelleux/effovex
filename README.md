# Effovex

Effovex (pronounced “f of x”) is a software system for document preparation. This is created for a software enginnering project by Antoine Guilmin Crépon, Balthazar Patiachvili and Jean Abou Samra at M1 MPRI in ENS Paris-Saclay.

## Installation

To install and compile this project, you need to clone the repository, go in the folder then build the project with `cargo`.

```bash
$ git clone https://gitlab.crans.org/pigeonmoelleux/effovex.git
$ cd effovex/
$ cargo build
```

## How to run effovex

To run effovex on a specific .fx file, you can either run the project with cargo 

```bash
$ cargo run -- path/to/example/file.fx
```

or use the binary produced by cargo after the build.

```bash
$ cargo build
$ ./target/debug/effovex path/to/example/file.fx
```

The output file will be placed in the same directory as the input file.

## User manual

The complete user manual can be found at https://perso.crans.org/pigeonmoelleux/M1/G%C3%A9nie%20logiciel/User_manual.pdf.

The source files are in [docs/user_manual/](docs/user_manual).