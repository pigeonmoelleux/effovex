#!/bin/sh

set -eu

GITLAB_API="https://gitlab.crans.org/api/v4"
GITLAB_API_PROJECT="$GITLAB_API/projects/pigeonmoelleux%2Feffovex"

REF_NAME="master"
JOB_NAME="build_and_create_baseline"

# FIXME: Always downloads the latest test-baseline; likely want the one for the
# base commit of the current merge request.
ARTIFACT_URL="$GITLAB_API_PROJECT/jobs/artifacts/$REF_NAME/raw/baseline.tar.gz?job=$JOB_NAME"
echo "Downloading $ARTIFACT_URL ..."
wget --quiet -O baseline.tar.gz "$ARTIFACT_URL"

echo "Extracting baseline.tar.gz ..."
tar -xf baseline.tar.gz
