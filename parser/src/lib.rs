//! Effovex parser
//!
//! Provides the function `effovex_parser` to parse .fx files

#[macro_use]
extern crate pest_derive;

pub mod ast;
pub mod error;
pub mod parser;
mod pretty;

pub use crate::ast::{Declaration, DeclarationType, Showable, ShowableType, Type, AST};
pub use crate::error::{Loc, ParserError};
pub use crate::parser::effovex_parser;
