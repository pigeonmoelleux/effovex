use std::cell::RefCell;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::{fs, vec};

use pest::error::Error;
use pest::iterators::{Pair, Pairs};
use pest::Parser;

use crate::ast::{Declaration, DeclarationType, Showable, ShowableType, Type, AST};
use crate::error::{Loc, ParserError};

#[derive(Parser, Debug)]
#[grammar = "effovex.pest"]
struct EffovexParser;

/// Convert a pest error to a parser error
fn error_pest_to_effovex(err: Error<Rule>, file_path: &Path) -> ParserError {
    let pest_error = err.renamed_rules(|&rule| match rule {
        Rule::WHITESPACE => "space, tabulation, newline".to_owned(),
        Rule::alpha => "alpha character".to_owned(),
        Rule::arg => "argument".to_owned(),
        Rule::blank => "space, tabulation".to_owned(),
        Rule::called_arg => "argument".to_owned(),
        Rule::called_env => "environment".to_owned(),
        Rule::called_func => "function".to_owned(),
        Rule::declaration => "declaration".to_owned(),
        Rule::abstract_arg => "argument usage (beginning with #)".to_owned(),
        Rule::def_env => "environment declaration".to_owned(),
        Rule::plain_env => "Plain environment".to_owned(),
        Rule::def_func => "function declaration".to_owned(),
        Rule::digit => "number".to_owned(),
        Rule::doc_type => "document type".to_owned(),
        Rule::name => "name".to_owned(),
        Rule::not_showable => "not showable".to_owned(),
        Rule::output_fmt => "output format between PDF, PNG or SVG".to_owned(),
        Rule::restricted_name => "restricted names (\"begin\", \"end\", \"document\")".to_owned(),
        Rule::text => "plain text (not beginning with \"@\")".to_owned(),
        Rule::showable => {
            "function, environment, object, plain text (not beginning with \"@\")".to_owned()
        }
        Rule::COMMENT => unreachable!("Comments should not appear in the AST"),
        Rule::relative_path => "relative path".to_owned(),
        Rule::import => "importation".to_owned(),
        Rule::package => "package importation".to_owned(),
        Rule::document_aux => "imported document".to_owned(),
        Rule::document_decl => "imported declaration file".to_owned(),
        Rule::document => "document".to_owned(),
    });

    return ParserError::CannotParse(
        Loc::from((pest_error.line_col, PathBuf::from(file_path))),
        pest_error.variant.message().to_string(),
    );
}

/// Convert pest pair corresponding to a declaration token to `Declaration`
fn pest_pair_to_declaration(
    outer_pair: Pair<Rule>,
    file_path: &Path,
) -> Result<Declaration, ParserError> {
    match outer_pair.into_inner().peek() {
        Some(pair) => match pair.as_rule() {
            Rule::def_func => {
                let mut name: String = Default::default();
                let mut args: Vec<String> = vec![];
                let mut returned: Vec<Showable> = vec![];
                let location = Loc::from((&pair.as_span().start_pos(), PathBuf::from(file_path)));
                pair.into_inner()
                    .try_for_each(|inner_pair| {
                        match inner_pair.as_rule() {
                            Rule::name => name = inner_pair.as_str().to_owned(),
                            Rule::arg => args.push(
                                inner_pair.as_str()[1..inner_pair.as_str().len() - 1].to_owned(),
                            ),
                            Rule::plain_env => {
                                inner_pair.into_inner().try_for_each(|showable| {
                                    match showable.as_rule() {
                                        Rule::showable => {
                                            returned.push(pest_pair_to_showable(showable, file_path)?)
                                        },
                                        Rule::WHITESPACE => {
                                            returned.push(Showable::new(Loc::from((&showable.as_span().start_pos(), PathBuf::from(file_path))), ShowableType::Text(" ".to_owned()) ))
                                        }
                                        _ => unreachable!("Unusual environment beginning declaration : {:?} should not appear", showable.as_rule())
                                    }
                                    Ok(())
                                })?
                            }
                            Rule::WHITESPACE => {}
                            _ => unreachable!(
                                "Unusual function declaration : {:?} should not appear",
                                inner_pair.as_rule()
                            ),
                        }
                        Ok(())
                    })?;
                Ok(Declaration::DefFunc(location, name, args, returned))
            }
            Rule::def_env => {
                let mut name: String = Default::default();
                let mut args: Vec<String> = vec![];
                let mut showable_beg: Vec<Showable> = vec![];
                let mut showable_end: Vec<Showable> = vec![];
                let location = Loc::from((&pair.as_span().start_pos(), PathBuf::from(file_path)));
                let mut switch = false;
                pair
                        .into_inner()
                        .try_for_each(|inner_pair| {
                            match inner_pair.as_rule() {
                                Rule::name => name = inner_pair.as_str().to_owned(),
                                Rule::arg => args.push(
                                    inner_pair.as_str()[1..inner_pair.as_str().len() - 1]
                                        .to_owned(),
                                ),
                                Rule::plain_env => {
                                    inner_pair.into_inner().try_for_each(|showable| {
                                        match showable.as_rule() {
                                            Rule::showable => {
                                                if !switch {
                                                    showable_beg.push(pest_pair_to_showable(showable, file_path)?)
                                                } else {
                                                    showable_end.push(pest_pair_to_showable(showable, file_path)?)
                                                }
                                            },
                                            Rule::WHITESPACE => {
                                                if !switch {
                                                    showable_beg.push(Showable::new(Loc::from((&showable.as_span().start_pos(), PathBuf::from(file_path))),ShowableType::Text(" ".to_owned()) ))
                                                } else {
                                                    showable_end.push(Showable::new(Loc::from((&showable.as_span().start_pos(), PathBuf::from(file_path))),ShowableType::Text(" ".to_owned()) ))
                                                }
                                            }
                                            _ => unreachable!("Unusual environment beginning declaration : {:?} should not appear", showable.as_rule())
                                        }
                                        Ok(())
                                    })?;
                                    switch = true;
                                }
                                Rule::WHITESPACE => {}
                                _ => unreachable!(
                                    "Unusual environment declaration : {:?} should not appear",
                                    inner_pair.as_rule()
                                )
                            }
                            Ok(())
                        })?;
                Ok(Declaration::DefEnv(
                    location,
                    name,
                    args,
                    showable_beg,
                    showable_end,
                ))
            }
            _ => unreachable!(
                "Unusual declaration found : {:?} should not appear",
                pair.as_rule()
            ),
        },
        None => unreachable!("No declaration structure found"),
    }
}

/// Import auxiliary file containing declarations
fn import_declarations(
    file_path: PathBuf,
    current_folder: PathBuf,
) -> Result<Vec<Declaration>, ParserError> {
    let file_content = match fs::read_to_string(&current_folder.join(&file_path)) {
        Ok(text) => text,
        Err(_) => {
            return Err(ParserError::FileNotReadable(
                file_path.to_string_lossy().to_string(),
            ))
        }
    };

    let pairs = match EffovexParser::parse(Rule::document_decl, &file_content) {
        Ok(pairs) => pairs,
        Err(err) => return Err(error_pest_to_effovex(err, &file_path)),
    };

    let mut declarations: Vec<Declaration> = vec![];
    for pair in pairs {
        for inner_pair in pair.into_inner() {
            declarations.push(match pest_pair_to_declaration(inner_pair, &file_path) {
                Ok(declaration) => declaration,
                Err(err) => return Err(err),
            });
        }
    }

    Ok(declarations)
}

/// Convert pest pair corresponding to a showable token to `Showable`
fn pest_pair_to_showable(
    outer_pair: Pair<Rule>,
    file_path: &Path,
) -> Result<Showable, ParserError> {
    match outer_pair.into_inner().peek() {
        Some(pair) => match pair.as_rule() {
            Rule::called_func => {
                let mut name: String = Default::default();
                let mut args: Vec<Vec<Showable>> = vec![];
                let location = Loc::from((&pair.as_span().start_pos(), PathBuf::from(file_path)));
                pair.into_inner().try_for_each(|inner_pair| {
                    match inner_pair.as_rule() {
                        Rule::name => name = inner_pair.as_str().to_owned(),
                        Rule::called_arg => {
                            let mut arg: Vec<Showable> = vec![];
                            inner_pair.into_inner().try_for_each(|showable_pair| {
                                arg.push(pest_pair_to_showable(showable_pair, file_path)?);
                                Ok(())
                            })?;
                            args.push(arg);
                        }
                        _ => unreachable!(
                            "Unusual function call structure : {:?} should not appear",
                            inner_pair.as_rule()
                        ),
                    }
                    Ok(())
                })?;

                Ok(Showable::new(
                    location,
                    ShowableType::CalledFunc(name, args),
                ))
            }
            Rule::called_env => {
                let mut name: String = Default::default();
                let mut args: Vec<Vec<Showable>> = vec![];
                let mut inner_objects: Vec<Showable> = vec![];
                let location = Loc::from((&pair.as_span().start_pos(), PathBuf::from(file_path)));
                pair.into_inner()
                    .try_for_each(|inner_pair| {
                        match inner_pair.as_rule() {
                            Rule::arg => {
                                if !name.is_empty()
                                    && name
                                        != inner_pair.as_str()[1..inner_pair.as_str().len() - 1]
                                {
                                    let (line, col) = inner_pair.as_span().split().0.line_col();
                                    return Err(ParserError::BadEnvCall(
                                        Loc::new(line, col, PathBuf::from(file_path)),
                                        format!("Unmatched environment call : the environment given in @begin, \"{}\", is not the same as the one in @end, \"{}\"", name, &inner_pair.as_str()[1..inner_pair.as_str().len()-1]),
                                    ));
                                } else {
                                    name = inner_pair.as_str()[1..inner_pair.as_str().len() - 1]
                                        .to_owned()
                                }
                            }
                            Rule::called_arg => {
                                let mut arg: Vec<Showable> = vec![];
                                inner_pair
                                    .into_inner()
                                    .try_for_each(|showable_pair| {
                                        arg.push(pest_pair_to_showable(showable_pair, file_path)?);
                                        Ok(())
                                    })?;
                                args.push(arg)
                            }
                            Rule::showable => {
                                inner_objects.push(pest_pair_to_showable(inner_pair, file_path)?)
                            }
                            Rule::WHITESPACE => match inner_pair.as_str() {
                                " " | "\t" => inner_objects
                                    .push(Showable::new(Loc::from((&inner_pair.as_span().start_pos(), PathBuf::from(file_path))),ShowableType::Text(inner_pair.as_str().to_owned()))),
                                "\n" | "\r\n" | "\r" => {
                                    inner_objects.push(Showable::new(Loc::from((&inner_pair.as_span().start_pos(), PathBuf::from(file_path))), ShowableType::Newline))
                                }
                                _ => unreachable!("Bad whitespace character : {}", inner_pair.as_str()),
                            },
                            _ => unreachable!(
                                "Unusual environment call structure : {:?} should not appear",
                                inner_pair.as_rule()
                            ),
                        }
                        Ok(())
                    })?;
                Ok(Showable::new(
                    location,
                    ShowableType::CalledEnv(name, args, inner_objects),
                ))
            }
            Rule::text => Ok(Showable::new(
                Loc::from((&pair.as_span().start_pos(), PathBuf::from(file_path))),
                ShowableType::Text(whitespace_merger(pair.as_str())),
            )),
            Rule::plain_env => {
                let mut inner_showable_list = vec![];
                let location = Loc::from((&pair.as_span().start_pos(), PathBuf::from(file_path)));
                pair.into_inner().try_for_each(|inner_pair| {
                    match inner_pair.as_rule() {
                        Rule::showable => {
                            inner_showable_list.push(pest_pair_to_showable(inner_pair, file_path)?)
                        }
                        _ => {
                            unreachable!("Unusual structure found : {:?}", inner_pair.as_rule())
                        }
                    };
                    Ok::<(), ParserError>(())
                })?;
                Ok(Showable::new(
                    location,
                    ShowableType::CalledEnv("".to_owned(), vec![], inner_showable_list),
                ))
            }
            Rule::abstract_arg => Ok(Showable::new(
                Loc::from((&pair.as_span().start_pos(), PathBuf::from(file_path))),
                ShowableType::AbstractArg(pair.as_str()[1..pair.as_str().len()].to_owned()),
            )),
            _ => unreachable!("Unusual showable structure found : {:?}", pair.as_rule()),
        },
        None => unreachable!("No showable structure found"),
    }
}

/// Merge all non-newline whitespace list to a single space
fn whitespace_merger(text: &str) -> String {
    // Is true iff, when in a whitespace list, the first one has not been replaced by a space yet
    let mut whitespace_switch = true;

    let new_text: String = text
        .chars()
        .map(|c| {
            if c.is_whitespace() {
                if whitespace_switch {
                    whitespace_switch = false;
                    " ".to_owned()
                } else {
                    "".to_owned()
                }
            } else {
                whitespace_switch = true;
                c.to_string()
            }
        })
        .collect();

    new_text
}

/// Removes initial and final spaces in a string
fn space_remover(mut text: String) -> String {
    if !text.is_empty() && text.starts_with(' ') {
        text.replace_range(0..1, "");
    }
    if !text.is_empty() && text.ends_with(' ') {
        text.replace_range(text.len() - 1.., "");
    }

    text
}

/// Merge all consecutives Showable::Text occurences in showable_list then call whitespace_merger
fn text_merger(showable_list: Vec<Showable>) -> Vec<Showable> {
    let mut new_list: Vec<Showable> = vec![];
    let mut text_stack: String = Default::default();
    let mut text_beginning: Loc = Default::default();

    for showable in showable_list {
        match showable.showable_type {
            ShowableType::Text(text) => {
                if text_beginning == Loc::default() {
                    text_beginning = showable.location;
                }
                text_stack.push_str(&text)
            }
            ShowableType::Newline => {
                if !text_stack.is_empty() {
                    new_list.push(Showable::new(
                        text_beginning,
                        ShowableType::Text(space_remover(whitespace_merger(&text_stack))),
                    ));
                    text_stack = Default::default();
                    text_beginning = Default::default();
                }
                new_list.push(showable);
            }
            ShowableType::AbstractArg(name) => {
                if !text_stack.is_empty() {
                    new_list.push(Showable::new(
                        text_beginning,
                        ShowableType::Text(space_remover(whitespace_merger(&text_stack))),
                    ));
                    text_stack = Default::default();
                    text_beginning = Default::default();
                }
                new_list.push(Showable::new(
                    showable.location,
                    ShowableType::AbstractArg(name),
                ));
            }
            ShowableType::CalledFunc(name, called_args) => {
                if !text_stack.is_empty() {
                    new_list.push(Showable::new(
                        text_beginning,
                        ShowableType::Text(space_remover(whitespace_merger(&text_stack))),
                    ));
                    text_stack = Default::default();
                    text_beginning = Default::default();
                }
                new_list.push(Showable::new(
                    showable.location,
                    ShowableType::CalledFunc(
                        name,
                        called_args.into_iter().map(text_merger).collect(),
                    ),
                ));
            }
            ShowableType::CalledEnv(name, args, shown) => {
                if !text_stack.is_empty() {
                    new_list.push(Showable::new(
                        text_beginning,
                        ShowableType::Text(space_remover(whitespace_merger(&text_stack))),
                    ));
                    text_stack = Default::default();
                    text_beginning = Default::default();
                }
                new_list.push(Showable::new(
                    showable.location,
                    ShowableType::CalledEnv(
                        name,
                        args.into_iter().map(text_merger).collect(),
                        text_merger(shown),
                    ),
                ));
            }
        }
    }

    if !text_stack.is_empty() {
        new_list.push(Showable::new(
            text_beginning,
            ShowableType::Text(space_remover(whitespace_merger(&text_stack))),
        ));
    }

    new_list
}

/// Removes one NEWlINE token for each sequence of such token and return a new vec
fn newline_remover(mut showable_list: Vec<Showable>) -> Vec<Showable> {
    // Is true iff, when in a newline list, no token has been removed yet
    let mut newline_switch = true;

    for showable in &mut showable_list {
        match &showable.showable_type {
            ShowableType::Newline => {
                if newline_switch {
                    newline_switch = false;
                    *showable = Showable::new(
                        showable.location.to_owned(),
                        ShowableType::Text(" ".to_owned()),
                    );
                }
            }
            ShowableType::Text(text) => {
                if !text.chars().all(|c| c.is_whitespace()) {
                    newline_switch = true;
                }
            }
            _ => {
                newline_switch = true;
            }
        }
    }

    showable_list
}

/// Improve the AST given in input, and returns a better version with the same semantic, with following changes :
///
/// * Removes one NEWlINE token for each sequence of such token
/// * Merges all consecutive Showable::Text occurrences and all non-newline whitespaces
fn ast_improver(ast: AST) -> Result<AST, ParserError> {
    let mut improved_ast = AST {
        doc_type: ast.doc_type,
        imported_packages: ast.imported_packages,
        declarations: HashMap::new(),
        shown: vec![],
    };

    for ((_, _), boxed_dec) in ast.declarations {
        match boxed_dec {
            Declaration::DefFunc(loc, name, args, returned) => {
                match improved_ast.declarations.insert(
                    (DeclarationType::Function, name.clone()),
                    Declaration::DefFunc(loc, name, args, text_merger(newline_remover(returned))),
                ) {
                    None => {}
                    Some(_) => {
                        unreachable!("Already Defined Declaration Error happened but has already been tested")
                    }
                }
            }
            Declaration::DefEnv(loc, name, args, beg, end) => {
                match improved_ast.declarations.insert(
                    (DeclarationType::Environment, name.clone()),
                    Declaration::DefEnv(
                        loc,
                        name,
                        args,
                        text_merger(newline_remover(beg)),
                        text_merger(newline_remover(end)),
                    ),
                ) {
                    None => {}
                    Some(_) => {
                        unreachable!("Already Defined Declaration Error happened but has already been tested")
                    }
                }
            }
        }
    }

    improved_ast.shown = text_merger(newline_remover(ast.shown));

    Ok(improved_ast)
}

/// Convert pest pairs corresponding to one pair document to `AST`
fn pest_pairs_to_ast(
    pairs: &Pairs<'_, Rule>,
    file_path: &Path,
) -> Result<RefCell<AST>, ParserError> {
    let mut ast = AST {
        doc_type: Type::PDF,
        imported_packages: vec![],
        declarations: HashMap::new(),
        shown: vec![],
    };

    match pairs.peek() {
        Some(document) => {
            document.into_inner().try_for_each(|pair| {
                match pair.as_rule() {
                    Rule::doc_type => {
                        pair.into_inner()
                            .for_each(|doc_type| match doc_type.as_str() {
                                "PDF" | "pdf" => ast.doc_type = Type::PDF,
                                "PNG" | "png" => ast.doc_type = Type::PNG,
                                "SVG" | "svg" => ast.doc_type = Type::SVG,
                                _ => unreachable!("Invalid document type"),
                            })
                    }
                    Rule::declaration => {
                        let declaration = pest_pair_to_declaration(pair, file_path)?;
                        let location = declaration.location();
                        match ast.declarations.insert((DeclarationType::from(&declaration), declaration.name()), declaration) {
                            None => {}
                            Some(old_declaration) => return Err(ParserError::AlreadyDefinedDeclaration(location, old_declaration.location(), DeclarationType::from(&old_declaration), old_declaration.name()))
                        }
                    }
                    Rule::called_env => {
                        pair.into_inner().try_for_each(|inner_pair| {
                            match inner_pair.as_rule() {
                                Rule::arg => {
                                    if inner_pair.as_str() != "(document)" {
                                        return Err(ParserError::MissingBeginDocument(Loc::from((&inner_pair.as_span().start_pos(), PathBuf::from(file_path)))))
                                    }
                                }
                                Rule::called_arg => {
                                    return  Err(ParserError::WrongSignature(Loc::from((&inner_pair.as_span().start_pos(), PathBuf::from(file_path))), DeclarationType::Environment, "document".to_owned(), 0, 1));
                                }
                                Rule::showable => {
                                    let showable = pest_pair_to_showable(inner_pair, file_path);
                                    ast.shown.push(showable?);
                                }
                                Rule::WHITESPACE => {
                                    match inner_pair.as_str() {
                                        " " | "\t" => ast.shown.push(Showable::new(Loc::from((&inner_pair.as_span().start_pos(), PathBuf::from(file_path))), ShowableType::Text(inner_pair.as_str().to_owned()))),
                                        "\n" | "\r\n" | "\r" => ast.shown.push(Showable::new(Loc::from((&inner_pair.as_span().start_pos(), PathBuf::from(file_path))), ShowableType::Newline)),
                                        _ => unreachable!("Bad whitespace character : {}", inner_pair.as_str())
                                    }
                                }
                                _ => unreachable!("Invalid document environment structure : {:?} should not appear", inner_pair.as_rule()),
                            };
                            Ok(())
                        })?;
                    }
                    Rule::import => {
                        let location = Loc::from((&pair.as_span().start_pos(), PathBuf::from(file_path)));
                        match pair.into_inner().peek() {
                            None => unreachable!("Invalid import structure : no path found"),
                            Some(inner_pair) => match inner_pair.as_rule() {
                                Rule::relative_path => {
                                    let declarations = import_declarations(PathBuf::from(inner_pair.as_str()), match file_path.parent() {
                                        None => return Err(ParserError::FileNotReadable(inner_pair.as_str().to_owned())),
                                        Some(parent) => PathBuf::from(parent)
                                    })?;
                                    // Only first?
                                    if let Some(declaration) = declarations.into_iter().next() {
                                        match ast.declarations.insert((DeclarationType::from(&declaration), declaration.name()), declaration) {
                                            None => {}
                                            Some(old_declaration) => return Err(ParserError::AlreadyDefinedDeclaration(location, old_declaration.location(), DeclarationType::from(&old_declaration), old_declaration.name()))
                                        }
                                    };
                                    return Ok(());
                                }
                                _ => unreachable!("Invalid relative path : {:?} should not appear", inner_pair.as_rule())
                            }
                        }
                    }
                    Rule::package => {
                        ast.imported_packages.push(match pair.into_inner().peek() {
                            Some(path) => path.as_str().to_owned(),
                            None => unreachable!("Invalid import structure : no path found")
                        });
                    }
                    Rule::WHITESPACE => {}
                    _ => unreachable!("Unusual document structure : {:?} should not appear", pair.as_rule()),
                }
                Ok(())
            })?;
        }
        None => unreachable!("Document structure not found"),
    }

    ast = ast_improver(ast)?;

    Ok(RefCell::new(ast))
}

/// Parse the given input into an AST
///
/// If there are several errors, only the first one will be shown in the output.
pub fn effovex_parser(file_path: PathBuf) -> Result<RefCell<AST>, ParserError> {
    let file_content = match fs::read_to_string(&file_path) {
        Ok(file_content) => file_content,
        Err(_) => {
            return Err(ParserError::FileNotReadable(
                file_path.to_string_lossy().to_string(),
            ))
        }
    };

    let parsed_file = EffovexParser::parse(Rule::document, &file_content);
    let pairs = match parsed_file {
        Ok(pairs) => Ok(pairs),
        Err(err) => Err(error_pest_to_effovex(err, &file_path)),
    }?;

    pest_pairs_to_ast(&pairs, &file_path)
}
