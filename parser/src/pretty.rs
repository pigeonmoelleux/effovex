use std::fmt::Display;

use crate::ast::{Declaration, Showable, ShowableType, Type, AST};

fn print_node(
    text: &str,
    f: &mut std::fmt::Formatter,
    indent: &Vec<bool>,
    is_last: bool,
) -> std::fmt::Result {
    write!(
        f,
        "{}\n{}\n",
        if !indent.is_empty() {
            indent[0..indent.len() - 1]
                .iter()
                .map(|b| if *b { "│   " } else { "    " })
                .collect::<String>()
                + "│   "
        } else {
            "".to_owned()
        },
        if !indent.is_empty() {
            indent[0..indent.len() - 1]
                .iter()
                .map(|b| if *b { "│   " } else { "    " })
                .collect::<String>()
                + if is_last {
                    "└───"
                } else {
                    "├───"
                }
                + text
        } else {
            text.to_owned()
        }
    )
}

fn print_declaration(
    declaration: &Declaration,
    f: &mut std::fmt::Formatter,
    indent: &mut Vec<bool>,
    is_last: bool,
) -> std::fmt::Result {
    match declaration {
        Declaration::DefFunc(_, name, args, returned) => {
            print_node(&("fn ".to_owned() + name), f, indent, is_last)?;
            indent.push(true);

            print_node("Arguments", f, indent, false)?;
            indent.push(true);
            if !args.is_empty() {
                for arg in &args[0..args.len() - 1] {
                    print_node(arg, f, indent, false)?;
                }
                indent.pop();
                indent.push(false);
                print_node(&args[args.len() - 1], f, indent, true)?;
            }
            indent.pop();

            indent.pop();
            indent.push(false);
            print_node("Returned", f, indent, true)?;
            indent.push(true);
            if !returned.is_empty() {
                for shown in returned[0..returned.len() - 1].iter().cloned() {
                    print_showable(shown, f, indent, false)?;
                }
                indent.pop();
                indent.push(false);
                print_showable(returned[returned.len() - 1].to_owned(), f, indent, true)?;
            }
            indent.pop();
            indent.pop();
        }
        Declaration::DefEnv(_, name, args, beg, end) => {
            print_node(&("env ".to_owned() + name), f, indent, is_last)?;
            indent.push(true);

            print_node("Arguments", f, indent, false)?;
            indent.push(true);
            if !args.is_empty() {
                for arg in &args[0..args.len() - 1] {
                    print_node(arg, f, indent, false)?;
                }
                indent.pop();
                indent.push(false);
                print_node(&args[args.len() - 1], f, indent, true)?;
            }
            indent.pop();

            print_node("Beginning", f, indent, false)?;
            indent.push(true);
            if !beg.is_empty() {
                for shown in beg[0..beg.len() - 1].iter().cloned() {
                    print_showable(shown, f, indent, false)?;
                }
                indent.pop();
                indent.push(false);
                print_showable(beg[beg.len() - 1].to_owned(), f, indent, true)?;
            }
            indent.pop();

            indent.pop();
            indent.push(false);
            print_node("Ending", f, indent, true)?;
            indent.push(true);
            if !end.is_empty() {
                for shown in end[0..end.len() - 1].iter().cloned() {
                    print_showable(shown, f, indent, false)?;
                }
                indent.pop();
                indent.push(false);
                print_showable(end[end.len() - 1].to_owned(), f, indent, true)?;
            }
            indent.pop();
            indent.pop();
        }
    };

    Ok(())
}

fn print_type(doc_type: Type, f: &mut std::fmt::Formatter, indent: &Vec<bool>) -> std::fmt::Result {
    match doc_type {
        Type::PDF => print_node("PDF", f, indent, false),
        Type::PNG => print_node("PNG", f, indent, false),
        Type::SVG => print_node("SVG", f, indent, false),
    }
}

fn print_showable(
    showable: Showable,
    f: &mut std::fmt::Formatter,
    indent: &mut Vec<bool>,
    is_last: bool,
) -> std::fmt::Result {
    match showable.showable_type {
        ShowableType::Newline => print_node("NEWLINE", f, indent, is_last)?,
        ShowableType::Text(text) => {
            print_node("Text", f, indent, is_last)?;
            indent.push(true);
            print_node(&format!("\"{}\"", &text), f, indent, true)?;
            indent.pop();
        }
        ShowableType::CalledFunc(name, called_args) => {
            print_node(&("Call fn ".to_owned() + &name), f, indent, is_last)?;
            indent.push(true);
            if !called_args.is_empty() {
                for called_arg in &called_args[0..called_args.len() - 1] {
                    print_node("Argument", f, indent, false)?;
                    indent.push(true);
                    if !called_arg.is_empty() {
                        for arg in called_arg[0..called_arg.len() - 1].iter().cloned() {
                            print_showable(arg, f, indent, false)?;
                        }
                        indent.pop();
                        indent.push(false);
                        print_showable(
                            called_arg[called_arg.len() - 1].to_owned(),
                            f,
                            indent,
                            true,
                        )?;
                    }
                    indent.pop();
                }

                indent.pop();
                indent.push(false);
                print_node("Argument", f, indent, true)?;
                let called_arg = &called_args[called_args.len() - 1];
                indent.push(true);
                if !called_arg.is_empty() {
                    for arg in called_arg[0..called_arg.len() - 1].iter().cloned() {
                        print_showable(arg, f, indent, false)?;
                    }
                    indent.pop();
                    indent.push(false);
                    print_showable(called_arg[called_arg.len() - 1].to_owned(), f, indent, true)?;
                }
                indent.pop();
            }
            indent.pop();
        }
        ShowableType::CalledEnv(name, args, shown) => {
            print_node(&("Call env ".to_owned() + &name), f, indent, is_last)?;
            indent.push(true);
            if !args.is_empty() {
                for called_arg in &args[0..args.len() - 1] {
                    print_node("Argument", f, indent, false)?;
                    indent.push(true);
                    if !called_arg.is_empty() {
                        for arg in called_arg[0..called_arg.len() - 1].iter().cloned() {
                            print_showable(arg, f, indent, false)?;
                        }
                        indent.pop();
                        indent.push(false);
                        print_showable(
                            called_arg[called_arg.len() - 1].to_owned(),
                            f,
                            indent,
                            true,
                        )?;
                    }
                    indent.pop();
                }

                print_node("Argument", f, indent, false)?;
                let called_arg = &args[args.len() - 1].to_owned();
                indent.push(true);
                if !called_arg.is_empty() {
                    for arg in called_arg[0..called_arg.len() - 1].iter().cloned() {
                        print_showable(arg, f, indent, false)?;
                    }
                    indent.pop();
                    indent.push(false);
                    print_showable(called_arg[called_arg.len() - 1].to_owned(), f, indent, true)?;
                }
                indent.pop();
            }

            indent.pop();
            indent.push(false);
            print_node("Shown", f, indent, true)?;
            indent.push(true);
            if !shown.is_empty() {
                for shw in shown[0..shown.len() - 1].iter().cloned() {
                    print_showable(shw, f, indent, false)?;
                }
                indent.pop();
                indent.push(false);
                print_showable(shown[shown.len() - 1].to_owned(), f, indent, true)?;
            }
            indent.pop();

            indent.pop();
        }
        ShowableType::AbstractArg(name) => {
            print_node("Abstract argument", f, indent, is_last)?;
            indent.push(true);
            print_node(&name, f, indent, true)?;
            indent.pop();
        }
    }

    Ok(())
}

impl Display for AST {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut indent: Vec<bool> = vec![];

        print_node("AST", f, &indent, false)?;
        indent.push(true);

        print_type(self.doc_type, f, &indent)?;

        print_node("Declarations", f, &indent, false)?;
        indent.push(true);
        if !self.declarations.is_empty() {
            let mut counter = 0;
            for ((_, _), dec) in &self.declarations {
                counter += 1;
                if counter == self.declarations.len() {
                    indent.pop();
                    indent.push(false);
                    print_declaration(&dec.to_owned(), f, &mut indent, true)?;
                } else {
                    print_declaration(&dec.to_owned(), f, &mut indent, false)?;
                }
            }
        }
        indent.pop();

        indent.pop();
        indent.push(false);
        print_node("Showables", f, &indent, true)?;
        indent.push(true);
        if !self.shown.is_empty() {
            for showable in self.shown[0..self.shown.len() - 1].iter().cloned() {
                print_showable(showable, f, &mut indent, false)?;
            }
            indent.pop();
            indent.push(false);
            print_showable(
                self.shown[self.shown.len() - 1].to_owned(),
                f,
                &mut indent,
                true,
            )?;
        }

        Ok(())
    }
}
