use std::collections::HashMap;

use crate::error::Loc;

/// Document type of the output
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Type {
    PDF,
    SVG,
    PNG,
}

/// Declarations made in the top of the document
#[derive(Clone, PartialEq, Eq)]
pub enum Declaration {
    /// Declaration of a function
    ///
    /// `DefFunc(name, args, returned)` is such that :
    ///
    /// * `loc` is the location of the environment in the input file
    /// * `name` is the name of the function defined
    /// * `args` is the list of the arguments taken by the function
    /// * `returned` is the list of the returned `Showable`
    DefFunc(Loc, String, Vec<String>, Vec<Showable>),

    /// Declaration of a environment
    ///
    /// `DefEnv(loc, name, args, beg, end)` is such that :
    ///
    /// * `loc` is the location of the environment in the input file
    /// * `name` is the name of the environment defined
    /// * `args` is the list of the arguments taken by the environment
    /// * `beg` is the list of the showable objects at the environment beginning
    /// * `end` is the list of the showable objects at the environment ending
    DefEnv(Loc, String, Vec<String>, Vec<Showable>, Vec<Showable>),
}

/// Objects that can be displayed in the final document
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum ShowableType {
    /// Plain text
    Text(String),

    /// New line
    Newline,

    /// Usage of an abstract argument
    ///
    /// `AbstractArg(name)` correspond to #name in the .fx file
    ///
    /// This is a special showable as it can only be used in function and environment declarations
    AbstractArg(String),

    /// Called function
    ///
    /// `CalledFunc(name, args)` is such that :
    ///
    /// - `name` is the name of the function
    ///
    /// - `args` is the list of the arguments given
    CalledFunc(String, Vec<Vec<Showable>>),

    /// Called environment
    ///
    /// `CalledEnv(name, args, shown)` is such that :
    ///
    /// - `name` is the name of the function
    ///
    /// - `args` is the list of the arguments given
    ///
    /// - `shown` is the list of the `Showable` objects in the environment
    CalledEnv(String, Vec<Vec<Showable>>, Vec<Showable>),
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Showable {
    pub location: Loc,
    pub showable_type: ShowableType,
}

/// Enumeration of possible declaration types
///
/// Only two values are possible : Function and Environment
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum DeclarationType {
    Function,
    Environment,
}

/// Abstract syntax tree corresponding to the output of the parsing phase
#[derive(Clone, PartialEq, Eq)]
pub struct AST {
    /// Type of the document
    pub doc_type: Type,

    /// List of imported packages
    ///
    /// The resolution of importations will occurre during conversion phase
    pub imported_packages: Vec<String>,

    /// Hashmap of declared functions and environment
    ///
    /// Each declaration is accessible from the unique key (declaration type, declaration name)
    pub declarations: HashMap<(DeclarationType, String), Declaration>,

    /// List of displayed objects in the final document
    pub shown: Vec<Showable>,
}

impl ToString for Type {
    fn to_string(&self) -> String {
        match self {
            Type::PDF => "pdf".to_owned(),
            Type::PNG => "png".to_owned(),
            Type::SVG => "svg".to_owned(),
        }
    }
}

impl Showable {
    pub fn new(location: Loc, showable_type: ShowableType) -> Self {
        Showable {
            showable_type,
            location,
        }
    }
}

impl Declaration {
    pub fn location(&self) -> Loc {
        match self {
            Self::DefFunc(loc, _, _, _) => loc.clone(),
            Self::DefEnv(loc, _, _, _, _) => loc.clone(),
        }
    }

    pub fn name(&self) -> String {
        match self {
            Self::DefFunc(_, name, _, _) => name.clone(),
            Self::DefEnv(_, name, _, _, _) => name.clone(),
        }
    }
}

impl From<&Declaration> for DeclarationType {
    fn from(declaration: &Declaration) -> Self {
        match declaration {
            Declaration::DefFunc(_, _, _, _) => DeclarationType::Function,
            Declaration::DefEnv(_, _, _, _, _) => DeclarationType::Environment,
        }
    }
}
