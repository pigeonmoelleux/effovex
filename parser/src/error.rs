use std::{fmt::Display, path::PathBuf};

use pest::{error::LineColLocation, Position};

pub use crate::ast::DeclarationType;

/// Location structure
///
/// Contains the line and the column of a character in the file
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Loc {
    pub line: usize,
    pub column: usize,
    pub file: PathBuf,
}

/// Parser errors enumeration
///
/// Each error type correspond to an error that can occurre during the parsing
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ParserError {
    /// `AlreadyDefinedDeclaration(loc, previous_loc, declaration_type, name)` : the declaration `name` of type `declaration_type` has already been defined at `previous_loc` but is redefined at `loc`
    AlreadyDefinedDeclaration(Loc, Loc, DeclarationType, String),

    /// `BadEnvCall(loc, msg)` : the environment given in @begin is not the same as the one in @end, and `loc` is the location of the @end marker
    BadEnvCall(Loc, String),

    /// `CannotParse(loc, msg)` : cannot parse the file because of the expression starting at `loc`
    CannotParse(Loc, String),

    /// `FileNotReadable(file_path)` : the file at `file_path` is not readable
    FileNotReadable(String),

    /// `MissingBeginDocument(loc)` : the element showable at `loc` is not placed inside the environment "document"
    MissingBeginDocument(Loc),

    /// `WrongSignature(loc, declaration_type, name, nb_args_expected, nb_args_given)` : the declaration `name` of type `declaration type` has been called with `nb_args_given` arguments, but `nb_args_expected` were given at `loc`
    WrongSignature(Loc, DeclarationType, String, usize, usize),
}

impl Loc {
    /// Creates a new Loc structure from a line and a column
    pub fn new(line: usize, column: usize, file: PathBuf) -> Self {
        Loc { line, column, file }
    }
}

impl From<(LineColLocation, PathBuf)> for Loc {
    fn from((location, file_path): (LineColLocation, PathBuf)) -> Self {
        match location {
            LineColLocation::Pos((line, col)) => Loc::new(line, col, file_path),
            LineColLocation::Span(lc1, lc2) => unreachable!(
                "Unknown use of pest library : error location given as a span : {:?} / {:?}",
                lc1, lc2
            ),
        }
    }
}

impl From<(&Position<'_>, PathBuf)> for Loc {
    fn from((pos, file): (&Position, PathBuf)) -> Self {
        let (line, col) = pos.line_col();
        Loc::new(line, col, file)
    }
}

impl Default for Loc {
    fn default() -> Self {
        Loc::new(0, 0, PathBuf::from("."))
    }
}

impl Display for Loc {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Line {} Column {}", self.line, self.column)
    }
}

impl Display for DeclarationType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                DeclarationType::Function => "function",
                DeclarationType::Environment => "environment",
            }
        )
    }
}

impl Display for ParserError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::AlreadyDefinedDeclaration(loc, previous_loc, declaration_type, name) =>
                    format!(
                        "Already Defined : at {} : the {} named {} has already been defined at {}",
                        loc, declaration_type, name, previous_loc
                    ),
                Self::BadEnvCall(loc, msg) =>
                    format!("Bad environment call : at {} : {}", loc, msg),
                Self::CannotParse(loc, msg) => format!("Cannot Parse : at {} : {}", loc, msg),
                Self::FileNotReadable(file_path) =>
                    format!("File not readable : the file {} is not readable", file_path),
                Self::MissingBeginDocument(loc) => format!(
                    "Missing Begin Document : at {} : @begin(document) is missing",
                    loc
                ),
                Self::WrongSignature(loc, declaration_type, name, nb_args_expected, nb_args_given) =>
                format!("Wrong Signature : at {} : the {} named {} has been called with {} arguments but {} was/were expected", loc, declaration_type, name, nb_args_given, nb_args_expected)
    
            }
        )
    }
}
