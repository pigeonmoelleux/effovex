document(PNG)

fn foo(x) {
    "Cette fonction écrit: #x et laisse le reste du document s'écrire"
}

@begin(document)

On peut appeler cette fonction: @foo(14), puis continuer à écrire.

Par ailleurs, appeler la fonction deux fois: @foo(hqdsjfjkfs), et continuer, marche aussi.

@end(document)