document(PNG)

fn foo(x) {
    "Cette fonction écrit: #x et laisse le reste du document s'écrire"
}

@begin(document)

On peut appeler cette fonction: @foo(14), puis continuer à écrire.

@end(document)