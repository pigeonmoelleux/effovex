document(PNG)

package("maths")

@begin(document)

On peut intégrer des fractions dans du texte, comme ceci:
@begin(maths)
    x= 4 + @frac(@frac(16)(15))(@frac(14)(13)) + 3
@end(maths)
, ainsi, on peut écrire des maths.

@end(document)