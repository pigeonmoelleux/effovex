document(PDF)

fn foo(bar) {
    This is #bar.
}

import("decl_file_1.fxd")

@begin(document)

    Hello world! @foo(a test) @bar(works)

@end(document)