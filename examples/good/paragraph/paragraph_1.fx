document(PNG)

@begin(document)

    First paragraph
    
    Second paragraph which is really long to show that
    returning to the next line is not taken
    in account by Effovex : only empty lines are searched for text formatting.

@end(document)