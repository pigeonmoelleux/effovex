document(PNG)

fn foo {
    foo
}

fn bar(b)(a)(r) {
    bar is not #b #a #r
}

fn foo {
    Twice !
}

@begin(document)

    Hello world!

@end(document)