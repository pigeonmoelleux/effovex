document(PNG)

env foo {
    foo
}{
    bar
}

env bar(b)(a)(r) {
    bar is not #b #a #r
}{
    #r #a #b
}

env foo {
    Twice !
}{
    Third !
}

@begin(document)

    Hello world!

@end(document)