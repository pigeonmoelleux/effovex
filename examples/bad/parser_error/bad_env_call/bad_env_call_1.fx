document(pdf)

env foo {
    Bar
}{
    Boo
}

env bar {
    Foo
}{
    Boo
}

@begin(document)

    @begin(foo)
        @begin(bar)
            Coucou
        @end(foo)
    @end(bar)

@end(document)