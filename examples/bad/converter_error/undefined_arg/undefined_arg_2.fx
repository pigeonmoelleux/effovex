document(PNG)

env foo(bar) {
    Do you like #boo ?
} {
    I prefer #bar !
}

@begin(document)

    @begin(foo)(chocolate)

        Hello world!

    @end(foo)

@end(document)