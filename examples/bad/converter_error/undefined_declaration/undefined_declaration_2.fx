document(PNG)

fn foo {
    I am a function !
}

@begin(document)

    @begin(foo)

        Hello world!

    @end(foo)

@end(document)