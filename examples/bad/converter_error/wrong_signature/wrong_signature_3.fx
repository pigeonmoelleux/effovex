document(PNG)

env foo(bar) {
    Top of the bar
}{
    End of the  #bar
}

@begin(document)

    @begin(foo)(bar)(boo)

        Hello world!

    @end(foo)

@end(document)