document(PNG)

fn foo(bar)(boo) {
    I like #bar and #boo !
}

@begin(document)

    Hello world!

    I call @foo(cat)(dogs)(turtles) !

@end(document)