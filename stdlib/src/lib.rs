//! This crate gives the built-ins functions of Effovex
//!
//! It is loaded by every Effovex compilation.

extern crate cairo;
extern crate pango;
extern crate pangocairo;
extern crate parser;
extern crate share;

pub mod maths;

pub use maths::{frac, maths};
