#![allow(clippy::ptr_arg)]

use std::cmp::max;

use cairo::{Context, Rectangle};
use pango::{units_from_double, units_to_double};
use share::{structs::Displayable, ConverterError, EnvironmentEncapsulation};

/// Structure representing fractions.
#[derive(Debug)]
pub struct Frac {
    num: Vec<Box<dyn Displayable>>,
    den: Vec<Box<dyn Displayable>>,
}

impl Displayable for Frac {
    fn area(&self) -> Rectangle {
        let num_rect = self.num.area();
        let den_rect = self.den.area();
        let width = units_to_double(max(
            units_from_double(num_rect.width()),
            units_from_double(den_rect.width()),
        )) + 10.;
        let height = num_rect.height() + den_rect.height();
        let x = num_rect.x() - 5.;
        let y = num_rect.y();
        Rectangle::new(x, y, width, height)
    }

    fn starting_point(&self) -> (f64, f64) {
        (0., self.num.area().height())
    }

    fn display(
        &self,
        context: &Context,
        start: (f64, f64),
        _: bool,
        line_contents: &mut Vec<Box<dyn Displayable>>,
        rest: &mut Vec<Box<dyn Displayable>>,
    ) -> (f64, f64) {
        context.move_to(start.0, start.1);
        context.rel_move_to(
            (self.area().width() - self.num.area().width()) / 2.,
            -self.num.area().height() + self.num.starting_point().1,
        );

        // Impression numérateur
        self.num.display(
            context,
            context.current_point().unwrap(),
            false,
            &mut vec![],
            &mut self.num.to_owned(),
        );
        context.rel_move_to(
            -(self.area().width() + self.num.area().width()) / 2.,
            self.num.area().height() - self.num.starting_point().1,
        );
        // Impression barre de fraction
        context.set_line_width(1.);
        context.rel_line_to(self.area().width(), 0.);
        context.rel_move_to(
            -(self.area().width() + self.den.area().width()) / 2.,
            self.den.starting_point().1,
        );
        // Impression dénominateur
        self.den.display(
            context,
            context.current_point().unwrap(),
            false,
            &mut vec![],
            &mut self.den.to_owned(),
        );
        context.rel_move_to(
            (self.area().width() - self.den.area().width()) / 2.,
            -self.den.starting_point().1,
        );
        line_contents.push(self.clone_dyn());
        rest.remove(0);
        context.current_point().unwrap()
    }

    fn clone_dyn(&self) -> Box<dyn Displayable> {
        Box::new(Frac {
            num: self.num.clone(),
            den: self.den.clone(),
        })
    }
}

pub fn frac(
    args: &Vec<Box<dyn Displayable>>,
    environments: &mut EnvironmentEncapsulation,
) -> Result<Box<dyn Displayable>, ConverterError> {
    if environments.contains(&"maths".to_owned()) {
        Ok(Box::new(Frac {
            num: vec![args.get(0).unwrap().clone()],
            den: vec![args.get(1).unwrap().clone()],
        }))
    } else {
        Err(ConverterError::PackageError(
            "maths",
            "Tried to call `frac` outside a math environment",
        ))
    }
}

pub fn maths(
    _: &Vec<Box<dyn Displayable>>,
    inner_objects: &Vec<Box<dyn Displayable>>,
    environments: &mut EnvironmentEncapsulation,
) -> Result<Box<dyn Displayable>, ConverterError> {
    return if environments[0..environments.len() - 1].contains(&"maths".to_owned()) {
        Err(ConverterError::PackageError(
            "maths",
            "Tried to enter a maths environment inside a math environment",
        ))
    } else {
        Ok(inner_objects.clone_dyn())
    };
}
