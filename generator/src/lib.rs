//! Effovex PDF printer
//!
//! Provides a function to print a PDF file from the PrintableAST struct
//! from effovex/converter

extern crate cairo;
extern crate converter;
extern crate pango;
extern crate pangocairo;
extern crate parser;
extern crate share;

mod generator;

pub use crate::generator::print;
