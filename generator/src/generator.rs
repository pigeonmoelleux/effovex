#![allow(clippy::borrowed_box)]

use converter::{PrintableAST, HEIGHT};
use parser::Type;
use share::structs::Displayable;
use std::cell::RefCell;
use std::fs::File;
use std::path::Path;

fn new_page(
    context: &cairo::Context,
    obj: &Box<dyn Displayable>,
    cursor: (f64, f64),
) -> (f64, f64) {
    if (*obj).area().height() - obj.starting_point().1 + 20. > HEIGHT - cursor.1 {
        context.show_page().unwrap();
        context.move_to(40., 20.);
        return context.current_point().unwrap();
    }
    cursor
}

pub fn print(tree: RefCell<PrintableAST>, path: &Path) {
    let tree = tree.into_inner();
    let context = tree.context;
    context.move_to(40., 20.);
    println!("{}", context.operator());
    let mut cursor = context.current_point().unwrap();
    let mut line_contents = vec![];
    let mut rest = tree.shown.clone();
    for obj in tree.shown {
        cursor = obj.display(&context, cursor, true, &mut line_contents, &mut rest);
        cursor = new_page(&context, &obj, cursor);
    }
    context.stroke().expect("Failure while printing the file");

    if tree.doctype == Type::PNG {
        context
            .target()
            .write_to_png(&mut File::create(path).expect("Failed to create PNG file"))
            .expect("Failed to print PNG file");
    }
}
