use std::fmt::Debug;

use cairo::{Context, Rectangle};
use pango::{units_from_double, units_to_double, Layout};

/// `Displayable` is the trait necessary for all objects to be printed
pub trait Displayable: Debug {
    /// `area` shows the position and size of the object in the printed file
    fn area(&self) -> Rectangle;

    /// `starting point` shows the position at which the display function should
    /// writing, relative to the top left corner of the object's area.
    fn starting_point(&self) -> (f64, f64);

    /// `display` shows `cairo` what to do to print the object. It takes the
    /// starting position of the cursor and returns its ending position.
    fn display(
        &self,
        context: &Context,
        start: (f64, f64),
        in_paragraph: bool,
        line_contents: &mut Vec<Box<dyn Displayable>>,
        rest: &mut Vec<Box<dyn Displayable>>,
    ) -> (f64, f64);

    /// Clone a dynamic Displayable in a Box
    fn clone_dyn(&self) -> Box<dyn Displayable>;
}

pub type EnvironmentEncapsulation = Vec<String>;

impl Clone for Box<dyn Displayable> {
    fn clone(&self) -> Self {
        self.clone_dyn()
    }
}

impl Displayable for Vec<Box<dyn Displayable>> {
    fn area(&self) -> Rectangle {
        let x = self.get(0).unwrap().area().x();
        let y = units_to_double(
            self.iter()
                .map(|obj| units_from_double(obj.area().y()))
                .min()
                .unwrap(),
        );
        let width = self.iter().map(|obj| obj.area().width()).sum();
        let height = units_to_double(
            self.iter()
                .map(|obj| units_from_double(obj.area().height()))
                .max()
                .unwrap(),
        );
        Rectangle::new(x, y, width, height)
    }

    fn starting_point(&self) -> (f64, f64) {
        let dx = self.get(0).unwrap().starting_point().0;
        let dy = units_to_double(
            self.iter()
                .map(|obj| units_from_double(obj.starting_point().1))
                .max()
                .unwrap(),
        );
        (dx, dy)
    }

    fn display(
        &self,
        context: &Context,
        start: (f64, f64),
        in_paragraph: bool,
        line_contents: &mut Vec<Box<dyn Displayable>>,
        rest: &mut Vec<Box<dyn Displayable>>,
    ) -> (f64, f64) {
        let mut current_point = (start.0, start.1);
        for obj in self {
            current_point = obj.display(
                context,
                current_point,
                in_paragraph,
                line_contents,
                &mut self.to_owned(),
            );
        }
        line_contents.push(self.clone_dyn());
        rest.remove(0);
        current_point
    }

    fn clone_dyn(&self) -> Box<dyn Displayable> {
        Box::new(
            self.iter()
                .map(|element| element.clone_dyn())
                .collect::<Vec<Box<dyn Displayable>>>(),
        )
    }
}

impl Displayable for Layout {
    fn area(&self) -> Rectangle {
        let rect = self.extents().1;
        Rectangle::new(
            units_to_double(rect.x()),
            units_to_double(rect.y()),
            units_to_double(rect.width()),
            units_to_double(rect.height()),
        )
    }

    fn starting_point(&self) -> (f64, f64) {
        let first_char = self.index_to_pos(0);
        (0., units_to_double(first_char.height()) / 2.)
    }

    fn display(
        &self,
        context: &Context,
        start: (f64, f64),
        in_paragraph: bool,
        line_contents: &mut Vec<Box<dyn Displayable>>,
        rest: &mut Vec<Box<dyn Displayable>>,
    ) -> (f64, f64) {
        self.set_indent(0);
        context.move_to(
            start.0 - self.starting_point().0,
            start.1 - self.starting_point().1,
        );
        if in_paragraph {
            if start.0 == 40. {
                self.set_indent(units_from_double(30.));
            } else {
                self.set_indent(units_from_double(start.0 - 30.));
                context.move_to(40., start.1 - self.starting_point().1);
            }
        }
        pangocairo::show_layout(context, self);
        let last_char = self.index_to_pos(self.character_count() + 1);
        let last_line = self.line(self.line_count() - 1).unwrap().extents().1;
        context.rel_move_to(
            units_to_double(last_char.x() + last_char.width()),
            self.area().height() - units_to_double(last_line.height()) / 2.,
        );
        rest.remove(0);
        line_contents.push(self.clone_dyn());
        context.current_point().unwrap()
    }

    fn clone_dyn(&self) -> Box<dyn Displayable> {
        Box::new(self.clone())
    }
}

pub fn vec_displayable_cast(list: Vec<Box<dyn Displayable>>) -> Box<dyn Displayable> {
    Box::new(list)
}

pub fn vec_vec_displayable_cast(list: Vec<Vec<Box<dyn Displayable>>>) -> Vec<Box<dyn Displayable>> {
    list.iter()
        .map(|sub_list| vec_displayable_cast(sub_list.to_owned()))
        .collect()
}
