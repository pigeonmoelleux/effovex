use std::{fmt::Display, path::PathBuf};

use parser::{DeclarationType, error::Loc};

/// Converter errors enumeration
///
/// Each error type correspond to an error that can occurre during the conversion
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ConverterError {
    /// `AlreadyDefinedDeclaration(loc, previous_loc, declaration_type, name)` : the declaration `name` of type `declaration_type` has already been defined at `previous_loc` but is redefined at `loc`
    AlreadyDefinedDeclaration(Loc, Loc, DeclarationType, String),

    /// `BadDeclarationType(name, found_type, expected_type)` : The declaration `name` has type `expected_type` but has been called as a `found_type`
    BadDeclarationType(String, DeclarationType, DeclarationType),

    /// `CannotCreatePackage(path)` : Cannot found the function `_package_create` in `path`
    CannotCreatePackage(PathBuf),

    /// `NonLoadableDeclaration(package_name, declaration_type, name)` : the declaration `name` of type `declaration_type` should be loaded by `package_name` but does not exist in said package
    NonLoadableDeclaration(String, DeclarationType, String),

    /// `PackageError(package, message)` : the package `package` has returned an error containing `message`
    PackageError(&'static str, &'static str),

    /// `PackageLoadingFailed(path)` : Failed to load the package located at `path`
    PackageLoadingFailed(PathBuf),

    /// `UndefinedArg(loc, arg_name, declaration_name)` : the abstract arg with name `arg_name` is used at `loc` in the declaration `declaration_name` but never defined
    UndefinedArg(Loc, String, String),

    /// `UndefinedDeclaration(loc, declaration_type, name)` : the declaration `name` of type `declaration_type` is used at `loc` but never defined in the document
    UndefinedDeclaration(Loc, DeclarationType, String),

    /// `WrongSignature(loc, declaration_type, name, nb_args_expected, nb_args_given)` : the declaration `name` of type `declaration type` has been called with `nb_args_given` arguments, but `nb_args_expected` were given at `loc`
    WrongSignature(Loc, DeclarationType, String, usize, usize),
}

impl Display for ConverterError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ConverterError::AlreadyDefinedDeclaration(loc, previous_loc, declaration_type, name) =>
                write!(f, "Already Defined : at {} : the {} named {} has already been defined at {}", loc, declaration_type, name, previous_loc),
            ConverterError::BadDeclarationType(name, found_type, expected_type) =>
                write!(f, "Bad Declaration Type : The declaration {} has type {} but has been called as a {}", name, expected_type, found_type),
            ConverterError::CannotCreatePackage(path) =>
                write!(f, "Cannot Create Package : Needed function _package_create cannot be found in {}", path.to_string_lossy()),
            ConverterError::NonLoadableDeclaration(package_name, declaration_type, name) =>
                write!(f, "Non Loadable Declaration : the {} named {} is loaded by the package {} but does not exist in said package", declaration_type, name, package_name),
            ConverterError::PackageError(package_name, message) =>
                write!(f, "Package Error : the package {} returned the following message : \"{}\"", package_name, message),
            ConverterError::PackageLoadingFailed(path) =>
                write!(f, "Package Loading Failed : Failed to load the package {}", path.to_string_lossy()),
            ConverterError::UndefinedArg(loc, arg_name, declaration_name) =>
                write!(f, "Undefined Argument : at {} : the argument #{} is used but never defined in the declaration of {}", loc, arg_name, declaration_name),
            ConverterError::UndefinedDeclaration(loc, declaration_type, name) =>
                write!(f, "Undefined Declaration : at {} : the {} named {} is called but never defined in the document", loc, declaration_type, name),
            ConverterError::WrongSignature(loc, declaration_type, name, nb_args_expected, nb_args_given) =>
                write!(f, "Wrong Signature : at {} : the {} named {} has been called with {} arguments but {} was/were expected", loc, declaration_type, name, nb_args_given, nb_args_expected)
        }
    }
}
